This module provides a Previous/Next type of navigation for node entity type. 
It integrates with Weight module.

To use it simply:
1.  add Previous/Next Navigation block to any region 
    (if you are using Display Suite, you may add 
    Previous/Next Navigation field inside Content Type's 
    Manage Display tab)
2.  go to Previous/Next Navigation at 
    Configuration -> System -> Previous/Next Navigation Settings 
    (admin/config/system/previous_next_navigation) 
    to enable and set up ordering direction for navigation per Content Type

Now, navigate to this type of content (e.g. http://mysite.com/node/2) and you 
should find Previous/Next pager on the page (of course it will happen only in 
case there're more than 1 node of this type created)
