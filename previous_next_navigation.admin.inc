<?php
/**
 * @file
 * Administration form for the module.
 */

/**
 * Administration form for Previous/Next Navigation Settings.
 * 
 * @return array
 *   Return system_settings_form result
 */
function previous_next_navigation_settings() {
  $pnn = array();

  $pnn['node'] = array(
    '#type' => 'fieldset',
    '#title' => 'Node',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $settings = variable_get('previous_next_navigation', array());
  if (module_exists('weight')) {
    $weight = _weight_get_settings();
  }

  if (!isset($settings['node'])) {
    $settings['node'] = array();
  }
  foreach (_node_types_build()->names as $type => $name) {
    $pnn['node'][$type] = array(
      '#type' => 'fieldset',
      '#title' => $name,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $pnn['node'][$type]['enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enabled'),
      '#default_value' => $settings['node'][$type]['enabled'],
    );
    $pnn['node'][$type]['order']['nid'] = array(
      '#type' => 'select',
      '#title' => t('NID order'),
      '#options' => array(
        'ASC' => 'ASC',
        'DESC' => 'DESC',
      ),
      '#default_value' => $settings['node'][$type]['order']['nid'],
    );

    if (isset($weight[$type]) && $weight[$type]['enabled']) {
      $pnn['node'][$type]['order']['weight'] = array(
        '#type' => 'select',
        '#title' => t('Weight order'),
        '#options' => array(
          0 => t('- None- '),
          'ASC' => 'ASC',
          'DESC' => 'DESC',
        ),
        '#default_value' => $settings['node'][$type]['order']['weight'],
      );
    }
  }

  $return = array(
    '#tree' => TRUE,
    '#submit' => array(
      'previous_next_navigation_settings_submit',
    ),
    'previous_next_navigation' => $pnn,
  );
  return system_settings_form($return);
}

/**
 * Settings form submit callback function. 
 * 
 * Clearing cache items with PREVIOUS_NEXT_NAVIGATION_CACHE_CODE prefix.
 * 
 * @param array $form 
 *   Form structure.
 * @param array $form_state 
 *   Form state data.
 */
function previous_next_navigation_settings_submit($form, &$form_state) {
  cache_clear_all(sprintf('%s:', PREVIOUS_NEXT_NAVIGATION_CACHE_CODE), 'cache', TRUE);
}
